﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using System.Net.Http;

namespace Юле_Замена_Ссылки_В_Столбце_v2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async System.Threading.Tasks.Task runAsync()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            var xlApp = new Microsoft.Office.Interop.Excel.Application();

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(@"d:\Art\documents\KLIKO_DOC\Книга3.xlsx");
                button1.Enabled = false;
                var xlWorkbook = xlApp.Workbooks.Open(openFileDialog1.FileName);
                _Worksheet xlWorksheet = (_Worksheet)xlWorkbook.Sheets[1];
                var xlRange = xlWorksheet.UsedRange;

                var rowCount = xlRange.Rows.Count;
                var colCount = xlRange.Columns.Count;

                for (var i = 2; i <= rowCount; i++)
                {
                    var rangeCell = (Range)xlRange.Cells[i, 10];
                    if (rangeCell.Value != null)
                    {
                        string s = rangeCell.Value2.ToString();
                        Console.WriteLine(s);
                        var hyperlinks = rangeCell.Hyperlinks.OfType<Hyperlink>();

                        foreach (var h in hyperlinks)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.AppendFormat("text: {0}, address: {1} \r\n", h.TextToDisplay, h.Address);
                            //h.Address = "http://qqq.ru";
                            Console.WriteLine(sb.ToString());

                            try
                            {
                                HttpClient client = new HttpClient();
                                HttpRequestMessage r = new HttpRequestMessage();
                                r.RequestUri = new Uri(h.Address);
                                r.Method = HttpMethod.Get;
                                r.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36");
                                r.Headers.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
                                r.Headers.Add("Accept-Encoding", "gzip, deflate, br");
                                r.Headers.Add("Accept-Language", "en-US,en;q=0.9");
                                r.Headers.Add("Connection", "keep-alive");

                                HttpResponseMessage response = null;
                                try
                                {
                                    response = await client.SendAsync(r);
                                    //   response.EnsureSuccessStatusCode();
                                    Console.WriteLine();
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                               //  var   resp = e.Response as HttpWebResponse;
                                    Console.WriteLine();
                                }


                                //string responseUri = response.RequestMessage.RequestUri.ToString();
                                string responseUri = response.Headers.Location.ToString();


                                rangeCell.Hyperlinks.Delete();
                                xlWorksheet.Hyperlinks.Add(rangeCell, responseUri, Type.Missing, responseUri, responseUri);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                                Console.WriteLine();
                            }
                        }


                    }
                }

                xlWorkbook.Save();

                GC.Collect();
                GC.WaitForPendingFinalizers();

                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);

                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);

                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                button1.Enabled = true;
                MessageBox.Show("Готово!");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            runAsync();
        }
    }
}
